#include <stdio.h>
#include <stdlib.h>
#define MAX_TEXT 100
#define MAX_CARACTERS 100
#define MAX_DIGITS 10
#define MAX_CODI 100
#define MAX_REPETICIONS 50

int main()
{
    //Declaració de Variables
    char text[MAX_TEXT+1];
    char textCodificat[MAX_TEXT+1];
    char llistaCaracters[MAX_CARACTERS+1];
    int llistaRepeticions[MAX_REPETICIONS];
    char codis[MAX_CODI+1][MAX_DIGITS+1]={"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000","0001","0010","0011","0100","0101","0110"};
    char codi[MAX_DIGITS+1];
    int it, itc, ic, irepe, i, j, a;
    char aux, auxchar;

    //Inicialitzacio de text i llistes
    text[0]='\0';
    i=0;
    while(i<=MAX_CARACTERS){
        llistaCaracters[i]='\0';
        i++;
    }
    i=0;
    while(i<MAX_CARACTERS){
        llistaRepeticions[i]=0;
        i++;
    }
    //Demanar Text
    printf("Introdueix el text que vulguis:");
    scanf("%100[^\n]", text);
    it=0;

    while(text[it]!='\0'){
        //Recerca de caracters
        irepe=0;
        while(text[it]!=llistaCaracters[irepe] &&
              llistaCaracters[irepe]!='\0')irepe++;
        if(llistaCaracters[irepe]=='\0'){
            llistaCaracters[irepe]=text[it];
        }else{
              llistaRepeticions[irepe]++;
        }
        it++;
    }
    it=0;
    while(llistaCaracters[irepe]!='\0')irepe++;
    it=0;
    while(it<irepe){
        printf("\nEl caracter %c te %d repeticions", llistaCaracters[it], llistaRepeticions[it]);
        it++;
    }

    //Ordenacio de caracters amb metode de bombolla
    irepe=0;
    while(llistaCaracters[irepe]!='\0')irepe++;

    for(i=2; i<= irepe; i++){
        for(j=0; j<= irepe-i; j++){
            if(llistaRepeticions[j] < llistaRepeticions[j+1]){
                aux = llistaRepeticions[j];
                llistaRepeticions[j]= llistaRepeticions[j+1];
                llistaRepeticions[j+1]= aux;
                auxchar = llistaCaracters[j];
                llistaCaracters[j] = llistaCaracters[j+1];
                llistaCaracters[j+1]=auxchar;
            }
        }
    }

    it=0;
    printf("\n\nOrdre repeticio:");
    while(it<irepe){
        printf("\ncaracter %c te %d repeticions\n", llistaCaracters[it], llistaRepeticions[it]);
        it++;
    }
    printf("\nCaracters \t Repeticions \t Codi");
    it=0;
    //Aqui imprimeixo la llista de caracters de repeteicions i el codi que li tocaria a cada una
    while(it<irepe){
        printf("\n%c \t \t %d \t \t %s\n" ,llistaCaracters[it],llistaRepeticions[it],codis[it]);
        it++;
    }
    //Codificar
    it=0; a=0;
    while(text[it]!='\0'){
            i=0;
        while(llistaCaracters[i]!=text[it]){
            i++;
        }
        itc=0;
        while(codis[it][itc]!='\0'){
        textCodificat[a]=codis[it][itc];
        itc++; a++;
    }
    textCodificat[a]='$';
    a++; it++;
}
printf("\nel text codificat es: %s",textCodificat);

return 0;
}
